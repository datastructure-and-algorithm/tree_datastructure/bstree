/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tree;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 *
 * @author ACER
 */
public class BSTree<T extends Comparable<T>> {

    private Node<T> root = null;
    int countNode = 0;

    public boolean isEmpty() {
        return root == null;
    }

    //viết không là lấy height root
    public int height() {
        return height(root);
    }

    public boolean contain(T elem) {
        return contain(root, elem);
    }

    //ADD===========================================================
    public boolean add(T elem) {
        if (contain(elem) == true) {
            return false;
        }
        root = add(root, elem);
        countNode++;
//        System.out.println("Root: "+ root.info);
        return true;
    }

    private Node add(Node<T> node, T elem) {

        if (node == null) {
            node = new Node(elem, null, null);
            return node;
        } else if (elem.compareTo(node.info) > 0) {
            node.right = add(node.right, elem);
        } else if (elem.compareTo(node.info) < 0) {
            node.left = add(node.left, elem);
        }
        return node;
    }

//==============================================================
    public boolean remove(T elem) {
        if (!contain(elem)) {
            return false;
        }
        root = remove(root, elem);
        countNode--;
        return true;
    }

    //các kiểu duyệt =====================================================
    //BFS - Breadth : Duyệt từng tầng duyệt từ trên xuốg, từ trái -> phải
    //N-L-R
    public void preorder() {
        preorderRec(root);
    }

    //L-N-R
    public void inOrder() {
        inorderRec(root);
    }

    //L-R-N
    public void postorder() {
        postorderRec(root);
    }

    //Các kiểu xóa ======================================================
    public void CopyingDel() {

    }

    public void MergingDel() {

    }

    // AVL Tree========================
    public void balanced() {
        balanced(root);
    }

    private void balanced(Node<T> root) {
        int distance = height(root.left) - height(root.right);
        if (distance > 1) {
            T newRoot = maxLeft(root.left);

        }
    }

    //================================
    //PRIVATE
    private int height(Node<T> node) {
        if (node == null) {
            return 0;
        }
        return 1 + Math.max(height(node.left), height(node.right));
    }

    private boolean contain(Node<T> node, T elem) {
        if (node == null) {
            return false;
        }

        int result = elem.compareTo(node.info);

        if (result == 0) {
            return true;
        }

        if (result < 0) {
            return contain(node.left, elem);
        }

        return contain(node.right, elem);
    }

    public void SearchNode(T elem) {
        Node<T> idx = SearchNode(root, elem);
        System.out.println("IDX ne: ");
        System.out.println("In4: " + idx.info);
        System.out.println("In4 Left: " + idx.left.info);
        System.out.println("In4 Right: " + idx.right.info);
    }

    private Node<T> SearchNode(Node<T> node, T elem) {
        if (node == null) {
            return new Node(null, null, null);
        }

        int result = elem.compareTo(node.info);

        if (result == 0) {
            return node;
        }

        if (result < 0) {
            return SearchNode(node.left, elem);
        }

        return SearchNode(node.right, elem);
    }

    private Node remove(Node<T> node, T elem) {
        int result = elem.compareTo(node.info);
        if (result > 0) {
            node.right = remove(node.right, elem);
        } else if (result < 0) {
            node.left = remove(node.left, elem);
        } //Trai null, Phai null, ko null (HARD)
        else {
            if (node.left == null) {
                Node<T> rightNode = node.right; //luu de return
                node.info = null;
                node = null;
                return rightNode;
            } else if (node.right == null) {
                Node<T> leftNode = node.left;
                node.info = null;
                node = null;
                return leftNode;
            } else {
                T tmp = minRight(node.right);
                node.right = remove(node.right, tmp);
//                System.out.println("\ntmp= " + tmp);
//                System.out.println("\n9.r= " + node.right.info);
//                System.out.println("\n9= " + node.info);
                Node<T> tmpNode = new Node(tmp, node.left, node.right );
                node=null;
                return tmpNode  ;
            }
        }
        return node;
    }

    //min right
    private T minRight(Node<T> node) {
        while (node.left != null) {
            node = node.left;
        }
        return node.info;
    }

    //max Left
    private T maxLeft(Node<T> node) {
        while (node.right != null) {
            node = node.right;
        }
        return node.info;
    }

    //ROOT _ LEFT _RIGHT
    private void preorderRec(Node root) {
        if (root != null) {
            System.out.print(root.info + ", ");
            preorderRec(root.left);
            preorderRec(root.right);
        }
    }

    private void inorderRec(Node root) {
        if (root == null) {
            return;
        } else {
            inorderRec(root.left);
            System.out.printf(root.info + ", ");
            inorderRec(root.right);
        }
    }

    private void postorderRec(Node root) {
        if (root == null) {
            return;
        } else {
            postorderRec(root.left);
            postorderRec(root.right);
            System.out.printf(root.info + ", ");
        }
    }

}
